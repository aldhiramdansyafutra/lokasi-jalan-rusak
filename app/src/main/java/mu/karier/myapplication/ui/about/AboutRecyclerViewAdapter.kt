package mu.karier.myapplication.ui.about

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import mu.karier.myapplication.R
import mu.karier.myapplication.data.Faq
import mu.karier.myapplication.databinding.ItemAboutBinding
import mu.karier.myapplication.extension.collapse
import mu.karier.myapplication.extension.expand

/**
 * [RecyclerView.Adapter] that can display a [PlaceholderItem].
 * TODO: Replace the implementation with code for your data type.
 */
class AboutRecyclerViewAdapter(
    private val values: List<Faq>
) : RecyclerView.Adapter<AboutRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            ItemAboutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.binding.apply {
            tvNumber.text = "${(position + 1)}."
            tvAsk.text = item.ask
            tvAnswer.text = item.answer
        }
        holder.binding.container.setOnClickListener {
            val isExpanded = holder.binding.tvAnswer.isVisible
            if (isExpanded) {
                holder.binding.tvAnswer.collapse {
                    holder.binding.ivArrow.setImageResource(R.drawable.ic_arrow_down)
                }
            } else {
                holder.binding.tvAnswer.expand {
                    holder.binding.ivArrow.setImageResource(R.drawable.ic_arrow_up)
                }
            }
        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(val binding: ItemAboutBinding) : RecyclerView.ViewHolder(binding.root)

}