package mu.karier.myapplication.ui.locations

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import mu.karier.myapplication.data.Location
import mu.karier.myapplication.databinding.ItemFragmentLocationsBinding

class MyItemRecyclerViewAdapter(
    private val values: ArrayList<Location>
) : RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemFragmentLocationsBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.binding.apply {
            tvIconText.text = item.name
                .replace("puskesmas", "", ignoreCase = true)
                .replace("kelurahan", "", ignoreCase = true)
                .replace("kecamatan", "", ignoreCase = true)
                .trim()
                .substring(0, 1)
            tvAddress.text = item.address
            tvInformation.text = item.information
            tvTitle.text = item.name
        }
        holder.itemView.setOnClickListener {
            it.findNavController()
                .navigate(LocationsFragmentDirections.actionLocationListToLocationDetail(item))
        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(val binding: ItemFragmentLocationsBinding) :
        RecyclerView.ViewHolder(binding.root)

}