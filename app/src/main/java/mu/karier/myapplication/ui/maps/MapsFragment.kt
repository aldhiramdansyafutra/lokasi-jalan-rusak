package mu.karier.myapplication.ui.maps

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import coil.load
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetDialog
import mu.karier.myapplication.R
import mu.karier.myapplication.data.Location
import mu.karier.myapplication.databinding.BottomSheetLocationBinding
import mu.karier.myapplication.databinding.FragmentMapsBinding

class MapsFragment : Fragment() {

    private var locations = ArrayList<Location>()

    private var _binding: FragmentMapsBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    var markerListener: MarkerListener? = null

    private val callback = OnMapReadyCallback { googleMap ->
        /**
         * Manipulates the map once available.
         * This callback is triggered when the map is ready to be used.
         * This is where we can add markers or lines, add listeners or move the camera.
         * In this case, we just add a marker near Sydney, Australia.
         * If Google Play services is not installed on the device, the user will be prompted to
         * install it inside the SupportMapFragment. This method will only be triggered once the
         * user has installed Google Play services and returned to the app.
         */
        val jakarta = LatLng(-6.240653, 106.877541)
        locations.forEach { location ->
            googleMap.addMarker(
                MarkerOptions()
                    .position(LatLng(location.latitude, location.longitude))
                    .title(location.name)
                    .snippet(location.id.toString())
            )
        }
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(jakarta, 11f))
        googleMap.setOnMarkerClickListener { marker ->
            showBottomSheetLocation(getLocation(marker.snippet?.toInt() ?: 0))
            true
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        locations = Location.getLocations(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMapsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }

    private fun getLocation(id: Int): Location {
        var location = Location()
        locations.forEach { item ->
            if (item.id == id) {
                location = item
                return@forEach
            }
        }
        return location
    }

    private fun showBottomSheetLocation(location: Location) {
        val bottomSheet = BottomSheetDialog(requireContext())
        val viewBinding = BottomSheetLocationBinding.inflate(
            layoutInflater, binding.container, false
        )
        viewBinding.ivLocation.load(location.thumbnail) {
            error(R.drawable.bg_image_not_found)
            placeholder(R.drawable.ic_image_not_found)
            crossfade(true)
        }
        viewBinding.tvTitle.text = location.name
        viewBinding.btnToDetail.setOnClickListener {
            bottomSheet.dismiss()
            markerListener?.onClickDetail(location)
        }
        bottomSheet.setContentView(viewBinding.root)
        bottomSheet.show()
    }
}

interface MarkerListener {
    fun onClickDetail(location: Location)
}