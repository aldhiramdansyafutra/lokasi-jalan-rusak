package mu.karier.myapplication.ui.locations

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import coil.load
import mu.karier.myapplication.R
import mu.karier.myapplication.data.Location
import mu.karier.myapplication.databinding.FragmentHomeBinding
import mu.karier.myapplication.databinding.FragmentLocationDetailBinding
import android.content.Intent
import android.net.Uri


class LocationDetailFragment : Fragment() {
    private var _binding: FragmentLocationDetailBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    var location = Location()

    companion object {
        const val LOCATION = "location"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments.apply {
            location = this?.getParcelable(LOCATION) ?: Location()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLocationDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        location.apply {
            binding.tvInformation.text = this.information
            binding.tvAddress.text = this.address
            binding.tvLatlng.text = "${this.latitude}, ${this.longitude}"
            binding.tvName.text = this.name
            binding.ivThumbnail.load(this.thumbnail) {
                crossfade(true)
                placeholder(R.drawable.ic_image_not_found)
                error(R.drawable.bg_image_not_found)
            }
            binding.btnVisit.setOnClickListener {
                startActivity(startDirection(location.latitude, location.longitude))
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    fun startDirection(destLatitude: Double, destLongitude: Double): Intent {
        val url = "http://maps.google.com/maps?daddr=$destLatitude,$destLongitude"
        return Intent(Intent.ACTION_VIEW, Uri.parse(url))
    }
}