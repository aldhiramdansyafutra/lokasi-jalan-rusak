package mu.karier.myapplication.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import mu.karier.myapplication.R
import mu.karier.myapplication.data.Location
import mu.karier.myapplication.databinding.FragmentHomeBinding
import mu.karier.myapplication.extension.replaceChildFragment
import mu.karier.myapplication.ui.maps.MapsFragment
import mu.karier.myapplication.ui.maps.MarkerListener

class HomeFragment : Fragment(), MarkerListener {

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = MapsFragment()
        mapFragment.markerListener = this
        replaceChildFragment(R.id.frame_maps, mapFragment)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onClickDetail(location: Location) {
        this.findNavController()
            .navigate(HomeFragmentDirections.actionHomeToLocationDetail(location))
    }
}