package mu.karier.myapplication.data

import android.content.Context
import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.io.IOException
import java.io.InputStream
import java.nio.charset.Charset
import org.json.JSONArray
import org.json.JSONException

import org.json.JSONObject

@Parcelize
data class Location(
    var id: Int = 0,
    var name: String = "",
    var address: String = "",
    var information: String = "",
    var longitude: Double = 0.0,
    var latitude: Double = 0.0,
    var thumbnail: String = ""

) : Parcelable {

    companion object {
        fun getLocations(context: Context): ArrayList<Location> {
            val list: ArrayList<Location> = ArrayList()
            try {
                val obj = JSONObject(loadLocationJson(context))
                val data: JSONArray = obj.getJSONArray("data")
                val count = data.length()
                for (i in 0 until count) {
                    list.add(parse(data.getJSONObject(i)))
                }
            } catch (ex: JSONException) {
                ex.printStackTrace()
            }
            return list
        }

        private fun loadLocationJson(context: Context): String {
            var json = ""
            return try {
                val `is`: InputStream = context.assets.open("location.json")
                val size: Int = `is`.available()
                val buffer = ByteArray(size)
                `is`.read(buffer)
                `is`.close()
                json = String(buffer, Charset.forName("UTF-8"))
                json
            } catch (ex: IOException) {
                ex.printStackTrace()
                json
            }
        }

        private fun parse(location: JSONObject): Location {
            return Location(
                id = location.getInt("id"),
                name = location.getString("nama"),
                address = location.getString("alamat"),
                information = location.getString("keterangan"),
                longitude = location.getDouble("longitude"),
                latitude = location.getDouble("latitude"),
                thumbnail = location.getString("image")
            )
        }
    }
}