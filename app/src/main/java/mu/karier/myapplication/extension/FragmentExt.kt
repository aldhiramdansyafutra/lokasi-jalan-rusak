package mu.karier.myapplication.extension

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) {
    beginTransaction().func().commit()
}

fun Fragment.replaceChildFragment(containerViewId: Int, fragment: Fragment) {
    childFragmentManager.inTransaction {
        replace(containerViewId, fragment)
    }
}